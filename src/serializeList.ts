import {identityGenerator} from './functional.js'
import {makeContinuable} from './iterator.js'

type ElementSerializer<T> = (element: T) => Iterable<string>

export function serializeList(
    elements: Iterable<string>,
    elementSerializer?: ElementSerializer<string>,
): Iterable<string>
export function serializeList<T>(
    elements: Iterable<T>,
    elementSerializer: ElementSerializer<T>,
): Iterable<string>
export function* serializeList(
    elements: Iterable<string>,
    elementSerializer: ElementSerializer<string> = identityGenerator,
): Iterable<string> {
    const iterator = makeContinuable(elements)
    const firstResult = iterator.next()

    if (!firstResult.done)
        yield* elementSerializer(firstResult.value)

    for (const value of iterator) {
        yield ', '
        yield* elementSerializer(value)
    }
}
