export type Falsy = null | undefined | false | 0 | -0 | 0n | ''

export type TruthyTypesOf<T> = T extends Falsy ? never : T

export const noop = (...args: unknown[]): void => undefined

export const identity = <T>(x: T) => x

export function* identityGenerator<T>(x: T) {
    yield x
}

export const operatorSubtract = (n: number, m: number) => n - m
export const operatorMutliply = (n: number, m: number) => n * m
export const operatorDivide = (n: number, m: number) => n / m

export function operatorAdd(n: number, m: number): number
export function operatorAdd(n: string, m: string): string
export function operatorAdd(n: unknown, m: unknown): unknown {
    return <number>n + <number>m
}

export const operatorEqual = <T>(n: T, m: T) => n === m

export const applier = <A extends readonly unknown[], R>(func: (...args: [...A]) => R) =>
    (args: [...A]) => func(...args)
