import {operatorAdd} from './functional.js'
import {pipe, zip, every, reduce, makeContinuable, take} from './iterator.js'
import {
    unifyLists as unifyTermLists,
    serialize as serializeTerm,
    MetaMap, BoundMap, VariableTerm, Term, QuantifiedVariableTerm,
} from './term.js'
import {PredicateSpecification} from './predicateSpecification.js'
import {serializeList} from './serializeList.js'
import {Type} from './type.js'

const enum Precedence {
    most,
    not,
    '&',
    or,
    implies,
    iff,
    quantor,
    least,
}

export abstract class Formula {
    abstract unify(other: Formula, metaMap: MetaMap, boundMap: BoundMap): boolean
    abstract serialize(outerPrecedence?: Precedence, isLeftSide?: boolean): Iterable<string>
    abstract substitute(map: Map<Term, Term>): Formula

    // TODO make these readonly
    static True: TrueFormula
    static False: FalseFormula
    static Conjunction: typeof ConjunctionFormula
    static Disjunction: typeof DisjunctionFormula
    static Implication: typeof ImplificationFormula
    static Predicate: typeof PredicateFormula
    static Equality: typeof EqualityFormula
    static Type: typeof TypeFormula
    static Generalization: typeof GeneralizationFormula
    static Existence: typeof ExistenceFormula
}

export abstract class ConstantFormula extends Formula {
    unify(other: Formula, metaMap: MetaMap, boundMap: BoundMap) {
        return this.constructor === other.constructor
    }

    substitute() {
        return this
    }
}

export class TrueFormula extends ConstantFormula {
    static readonly instance = new TrueFormula

    private constructor() {
        super()
    }

    * serialize() {
        yield 'true'
    }
}

export class FalseFormula extends ConstantFormula {
    static readonly instance = new FalseFormula

    private constructor() {
        super()
    }

    * serialize() {
        yield 'false'
    }
}

export abstract class BinaryFormula extends Formula {
    private static readonly operationMap = {
        '&': Precedence['&'],
        or: Precedence.or,
        implies: Precedence.implies,
        iff: Precedence.iff,
    }

    readonly operands: [Formula, Formula]

    constructor(operands: [Formula, Formula]) {
        super()
        this.operands = operands
    }

    unify(other: Formula, metaMap: MetaMap, boundMap: BoundMap): boolean {
        if (this.constructor !== other.constructor)
            return false

        return pipe(
            zip(this.operands, (other as BinaryFormula).operands),
            every(([operand1, operand2]) => operand1.unify(operand2, metaMap, boundMap))
        )
    }

    substituteOperands(map: Map<Term, Term>): [Formula, Formula] {
        return [
            this.operands[0].substitute(map),
            this.operands[1].substitute(map),
        ]
    }

    protected static* serializeHelper(
        connective: '&' | 'or' | 'implies' | 'iff',
        operands: [Formula, Formula],
        outerPrecedence: Precedence,
        isLeftSide: boolean,
    ): Iterable<string> {
        const precedence = BinaryFormula.operationMap[connective]

        if (precedence > outerPrecedence || precedence == outerPrecedence && isLeftSide)
            yield '('

        yield* operands[0].serialize(precedence, true)
        yield ' '
        yield connective
        yield ' '
        yield* operands[1].serialize(precedence)

        if (precedence > outerPrecedence || precedence == outerPrecedence && isLeftSide)
            yield ')'
    }
}

export class ConjunctionFormula extends BinaryFormula {
    substitute(map: Map<Term, Term>) {
        return new ConjunctionFormula(this.substituteOperands(map))
    }

    * serialize(outerPrecedence = Precedence.least, isLeftSide = false) {
        const [left, right] = this.operands
        const boundMap: BoundMap = new Map()
        const metaMap: MetaMap = new Map()
        const isEquivalence = left instanceof ImplificationFormula && right instanceof ImplificationFormula &&
            left.operands[0].unify(right.operands[1], metaMap, boundMap) &&
            left.operands[1].unify(right.operands[0], metaMap, boundMap)

        yield* isEquivalence
            ? BinaryFormula.serializeHelper('iff', left.operands, outerPrecedence, isLeftSide)
            : BinaryFormula.serializeHelper('&', this.operands, outerPrecedence, isLeftSide)
    }
}

export class DisjunctionFormula extends BinaryFormula {
    substitute(map: Map<Term, Term>) {
        return new DisjunctionFormula(this.substituteOperands(map))
    }

    * serialize(outerPrecedence = Precedence.least, isLeftSide = false) {
        yield* BinaryFormula.serializeHelper('or', this.operands, outerPrecedence, isLeftSide)
    }
}

export class ImplificationFormula extends BinaryFormula {
    substitute(map: Map<Term, Term>) {
        return new ImplificationFormula(this.substituteOperands(map))
    }

    * serialize(outerPrecedence = Precedence.least, isLeftSide = false) {
        if (this.operands[1] == FalseFormula.instance) {
            yield 'not '
            yield* this.operands[0].serialize(Precedence.not)
        } else
            yield* BinaryFormula.serializeHelper('implies', this.operands, outerPrecedence, isLeftSide)
    }
}

export class PredicateFormula extends Formula {
    readonly specification: PredicateSpecification
    readonly parameters: Term[]

    constructor(specification: PredicateSpecification, parameters: Term[]) {
        super()
        this.specification = specification
        this.parameters = parameters
    }

    unify(other: Formula, metaMap: MetaMap, boundMap: BoundMap): boolean {
        return other instanceof PredicateFormula &&
            this.specification === other.specification &&
            unifyTermLists(this.parameters, other.parameters, metaMap, boundMap)
    }

    substitute(map: Map<Term, Term>) {
        return new PredicateFormula(
            this.specification,
            this.parameters.map(parameter => parameter.substitute(map))
        )
    }

    * serialize() {
        const parameterIterator = pipe(this.parameters, makeContinuable)
        const {symbolOffset, name} = this.specification

        yield* serializeList(
            take(symbolOffset)(parameterIterator),
            serializeTerm
        )

        if (symbolOffset > 0)
            yield ' '

        yield name

        if (this.specification.parameterTypes.length > symbolOffset)
            yield ' '

        yield* serializeList(parameterIterator, serializeTerm)
    }
}

export class EqualityFormula extends Formula {
    readonly left: Term
    readonly right: Term

    constructor(left: Term, right: Term) {
        super()
        this.left = left
        this.right = right
    }

    unify(other: Formula, metaMap: MetaMap, boundMap: BoundMap): boolean {
        return other instanceof EqualityFormula &&
            this.left.unify(other.left, metaMap, boundMap) &&
            this.right.unify(other.right, metaMap, boundMap)
    }

    substitute(map: Map<Term, Term>) {
        return new EqualityFormula(
            this.left.substitute(map),
            this.right.substitute(map),
        )
    }

    * serialize() {
        yield* serializeTerm(this.left)
        yield ' = '
        yield* serializeTerm(this.right)
    }
}

export class TypeFormula extends Formula {
    readonly type: Type
    readonly parameter: Term

    constructor(parameter: Term, type: Type) {
        super()
        this.type = type
        this.parameter = parameter
    }

    unify(other: Formula, metaMap: MetaMap, boundMap: BoundMap): boolean {
        return other instanceof TypeFormula &&
            this.type === other.type &&
            this.parameter.unify(other.parameter, metaMap, boundMap)
    }

    substitute(map: Map<Term, Term>) {
        return new TypeFormula(
            this.parameter.substitute(map),
            this.type,
        )
    }

    * serialize() {
        yield* serializeTerm(this.parameter)
        yield ' is '
        yield this.type.name
    }
}

export abstract class QuantorFormula extends Formula {
    variable: QuantifiedVariableTerm
    operand: Formula

    constructor(variable: QuantifiedVariableTerm, operand: Formula) {
        super()
        this.variable = variable
        this.operand = operand
    }

    unify(other: Formula, metaMap: MetaMap, boundMap: BoundMap): boolean {
        if (
            this.constructor !== other.constructor ||
            this.variable.type != (other as GeneralizationFormula).variable.type
        )
            return false

        boundMap.set(this.variable, (other as GeneralizationFormula).variable)

        return this.operand.unify((other as GeneralizationFormula).operand, metaMap, boundMap)
    }
}

export class GeneralizationFormula extends QuantorFormula {
    substitute(map: Map<Term, Term>) {
        return new GeneralizationFormula(
            this.variable.substitute(map) as QuantifiedVariableTerm,
            this.operand.substitute(map),
        )
    }

    * serialize(outerPrecedence: Precedence, isLeftSide: boolean): Iterable<string> {
        yield 'for '

        let formula = this as Formula

        yield* serializeList(
            (function* () {
                for (; formula instanceof GeneralizationFormula; formula = formula.operand)
                    yield formula.variable
            }()),
            serializeQuantifiedVariable,
        )

        if (formula instanceof ImplificationFormula) {
            yield ' st '
            yield* formula.operands[0].serialize(Precedence.quantor, true);

            [, formula] = formula.operands
        }

        yield formula instanceof ExistenceFormula
            ? ' '
            : ' holds '

        yield* formula.serialize(Precedence.quantor)
    }
}
export class ExistenceFormula extends QuantorFormula {
    substitute(map: Map<Term, Term>) {
        return new ExistenceFormula(
            // @ts-expect-error TODO
            this.variable.substitute(map),
            this.operand.substitute(map),
        )
    }

    * serialize(outerPrecedence: Precedence, isLeftSide?: boolean | undefined) {
        yield 'ex '

        // TODO consider review this assignment
        let formula = this as Formula

        yield* serializeList(
            (function* () {
                for (; formula instanceof ExistenceFormula; formula = formula.operand)
                    yield formula.variable
            }()),
            serializeQuantifiedVariable,
        )

        yield ' st '
        yield* formula.serialize(Precedence.quantor)
    }
}

export function* serialize(formula: Formula) {
    yield* formula.serialize()
}

export function* serializeQuantifiedVariable(variable: VariableTerm) {
    yield variable.name
    yield ' be '
    yield variable.type.name
}

export function stringify(formula: Formula) {
    return pipe(
        serialize(formula),
        reduce(operatorAdd)
    )
}

Object.assign(
    Formula,
    {
        True: TrueFormula.instance,
        False: FalseFormula.instance,
        Conjunction: ConjunctionFormula,
        Disjunction: DisjunctionFormula,
        Implication: ImplificationFormula,
        Predicate: PredicateFormula,
        Equality: EqualityFormula,
        Type: TypeFormula,
        Generalization: GeneralizationFormula,
        Existence: ExistenceFormula,
    }
)
