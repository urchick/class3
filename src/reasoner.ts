import {reverse} from './iterator.js'
import {Formula} from './formula.js'
import {FreeVariableTerm, QuantifiedVariableTerm} from './term.js'

function unifiable(formula1: Formula, formula2: Formula) {
    return formula1.unify(formula2, new Map, new Map)
}

type ThesisItem =
    | {type: 'let' | 'given' | 'take', variable: FreeVariableTerm}
    | {type: 'assume' | 'thus', formula: Formula}

export class Reasoner {
    thesis: Formula | undefined
    thesisItems: ThesisItem[] = []

    constructor(thesis?: Formula) {
        this.thesis = thesis
    }

    checkLet(variable: FreeVariableTerm) {
        this.thesisItems.push({type: 'let', variable})

        if (this.thesis)
            if (
                this.thesis instanceof Formula.Generalization &&
                this.thesis.variable.getType() === variable.getType()
            )
                this.thesis = this.thesis.operand.substitute(new Map([[this.thesis.variable, variable]]))
            else
                return false

        return true
    }

    checkGiven(variable: FreeVariableTerm) {
        this.thesisItems.push({type: 'given', variable})

        if (!this.thesis)
            return true

        if (!(this.thesis instanceof Formula.Implication))
            return false

        const [antecedent, consequent] = this.thesis.operands

        if (antecedent instanceof Formula.Existence && antecedent.variable.getType() === variable.getType())
            this.thesis = new Formula.Implication([antecedent.operand, consequent])
                .substitute(new Map([[antecedent.variable, variable]]))
        else
            return false

        return true
    }

    checkTake(variable: FreeVariableTerm) {
        this.thesisItems.push({type: 'take', variable})

        if (!this.thesis)
            return true

        if (!(this.thesis instanceof Formula.Existence))
            return false

        this.thesis = this.thesis.operand.substitute(new Map([[this.thesis.variable, variable]]))

        return true
    }

    checkAssume(formula: Formula) {
        this.thesisItems.push({type: 'assume', formula})

        return this.checkDirectAssume(formula) || this.checkIndirectAssume(formula)
    }

    checkDirectAssume(formula: Formula) {
        if (!this.thesis)
            return true

        if (!(this.thesis instanceof Formula.Implication))
            return false

        const [antecedent, consequent] = this.thesis.operands

        if (unifiable(antecedent, formula))
            this.thesis = consequent
        else if (antecedent instanceof Formula.Conjunction && unifiable(antecedent.operands[0], formula))
            this.thesis = new Formula.Implication([antecedent.operands[1], consequent])
        else
            return false

        return true
    }

    checkIndirectAssume(formula: Formula) {
        if (!this.thesis)
            return true

        if (formula instanceof Formula.Implication &&
            formula.operands[1] == Formula.False &&
            unifiable(formula.operands[0], this.thesis)
        ) {
            this.thesis = Formula.False
            return true
        }

        return false
    }

    checkThus(formula: Formula) {
        this.thesisItems.push({type: 'thus', formula})

        if (this.thesis)
            if (unifiable(this.thesis, formula))
                this.thesis = Formula.True
            else if (this.thesis instanceof Formula.Conjunction && unifiable(this.thesis.operands[0], formula))
                [, this.thesis] = this.thesis.operands
            else
                return false

        return true
    }

    getReasoningThesis() {
        const thesisItemIterator = reverse(this.thesisItems)
        let thesis: Formula
        const lastItem = this.thesisItems.at(-1)

        if (lastItem?.type == 'thus') {
            thesis = lastItem.formula
            thesisItemIterator.next()
        } else
            thesis = Formula.True

        const variableMap = new Map<FreeVariableTerm, QuantifiedVariableTerm>

        for (const thesisItem of thesisItemIterator)
            switch (thesisItem.type) {
            case 'assume':
                thesis = new Formula.Implication([thesisItem.formula, thesis])
                break

            case 'given': {
                const {variable} = thesisItem

                if (!variableMap.get(variable))
                    variableMap.set(variable, new QuantifiedVariableTerm(variable))

                if (!(thesis instanceof Formula.Implication))
                    thesis = new Formula.Implication([Formula.True, thesis])

                thesis = new Formula.Implication([
                    new Formula.Existence(
                        variable,
                        // @ts-expect-error thesis is Implication here
                        // eslint-disable-next-line max-len
                        // eslint-disable-next-line @typescript-eslint/no-unsafe-member-access, @typescript-eslint/no-unsafe-argument
                        thesis.operands[0]
                    ),
                    // @ts-expect-error thesis is Implication here
                    // eslint-disable-next-line @typescript-eslint/no-unsafe-member-access
                    thesis.operands[1] as Formula
                ])
                break
            }

            case 'let': {
                const {variable} = thesisItem

                if (!variableMap.get(variable))
                    variableMap.set(variable, new QuantifiedVariableTerm(variable))

                thesis = new Formula.Generalization(variable, thesis)
                break
            }

            case 'take': {
                const {variable} = thesisItem

                if (!variableMap.get(variable))
                    variableMap.set(variable, new QuantifiedVariableTerm(variable))

                thesis = new Formula.Existence(variable, thesis)
                break
            }

            case 'thus':
                thesis = new Formula.Conjunction([thesisItem.formula, thesis])
                break
            }

        return thesis.substitute(variableMap)
    }
}
