export type Type = {
    name: string
    baseTypes: Type[]
}

export const isSubtypeOf = (type1: Type, type2: Type): boolean =>
    type1 === type2 || type1.baseTypes.some(type => isSubtypeOf(type, type2))
