// eslint-disable-next-line etc/no-misused-generics
export function createObjectMap<K extends string | number | symbol, V>(): Record<K, V> {
    return Object.create(null) as Record<K, V>
}
